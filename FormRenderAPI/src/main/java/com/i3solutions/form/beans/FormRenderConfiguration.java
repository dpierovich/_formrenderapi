package com.i3solutions.form.beans;

public class FormRenderConfiguration {
	
	private String formType = "";
	private String assessmentRecordId = "";
	private String surveyRecordId = "";
	
	private boolean configSet = false;
	
	//Assessment type configuration
	private boolean includeAssessmentData = false;
    private String assessmentDataEndPointURL = "";
    private String assessmentFieldMappings = "";
    private String assessmentAuthType = "";
    
    //Survey config fields
    private boolean includeSurveyData = false;
    private String surveyDataEndPointURL = "";
    private String surveyFieldMappings = "";
    private String surveyAuthType = "";
    
    //Display Template config
    private String displayTemplate = "";
    private String displayTemplateType = "html";
    
    
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	
	public String getAssessmentRecordId() {
		return this.assessmentRecordId;
	}
	public void setAssessmentRecordId(String assessmentRecordId) {
		this.assessmentRecordId = assessmentRecordId;
	}

	public String getSurveyRecordId() {
		return this.surveyRecordId;
	}
	public void setSurveyRecordId(String surveyRecordId) {
		this.surveyRecordId = surveyRecordId;
	}

	public boolean isConfigSet() {
		return configSet;
	}
	
	public void setConfigSet(boolean configSet) {
		this.configSet = configSet;
	}
	public boolean includeAssessmentData() {
		return includeAssessmentData;
	}
	public void setIncludeAssessmentData(boolean includeAssessmentData) {
		this.includeAssessmentData = includeAssessmentData;
	}
	
	public String getAssessmentDataEndPointURL() {
		return assessmentDataEndPointURL;
	}
	public void setAssessmentDataEndPointURL(String assessmentDataEndPointURL) {
		this.assessmentDataEndPointURL = assessmentDataEndPointURL;
	}
	
	public String getAssessmentFieldMappings() {
		return assessmentFieldMappings;
	}
	public void setAssessmentFieldMappings(String assessmentFieldMappings) {
		this.assessmentFieldMappings = assessmentFieldMappings;
	}
	
	public String getAssessmentAuthType() {
		return assessmentAuthType;
	}
	public void setAssessmentAuthType(String assessmentAuthType) {
		this.assessmentAuthType = assessmentAuthType;
	}
	
	public boolean includeSurveyData() {
		return includeSurveyData;
	}
	public void setIncludeSurveyData(boolean includeSurveyData) {
		this.includeSurveyData = includeSurveyData;
	}
	
	public String getSurveyDataEndPointURL() {
		return surveyDataEndPointURL;
	}
	public void setSurveyDataEndPointURL(String surveyDataEndPointURL) {
		this.surveyDataEndPointURL = surveyDataEndPointURL;
	}
	
	public String getSurveyFieldMappings() {
		return surveyFieldMappings;
	}
	public void setSurveyFieldMappings(String surveyFieldMappings) {
		this.surveyFieldMappings = surveyFieldMappings;
	}
	
	public String getSurveyAuthType() {
		return surveyAuthType;
	}
	public void setSurveyAuthType(String surveyAuthType) {
		this.surveyAuthType = surveyAuthType;
	}
	
	public String getDisplayTemplate() {
		return displayTemplate;
	}
	public void setDisplayTemplate(String displayTemplate) {
		this.displayTemplate = displayTemplate;
	}
	
	public String getDisplayTemplateType() {
		return displayTemplateType;
	}
	public void setDisplayTemplateType(String displayTemplateType) {
		this.displayTemplateType = displayTemplateType;
	}
} // end class FormRenderConfiguration
