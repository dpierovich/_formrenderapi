package com.i3solutions.form.render.api;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

public class FormRenderApplication extends Application {
	
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> classes = new HashSet<Class<?>>();
		classes.add(FormRenderService.class);
		return classes;
	}

}
