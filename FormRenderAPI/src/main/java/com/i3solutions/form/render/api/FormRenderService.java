package com.i3solutions.form.render.api;

import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
//import java.net.URI;
import java.net.URL;
//import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
//import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
//import java.util.List;
import java.util.Locale;
//import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Map;

// Jackson
import org.codehaus.jackson.JsonNode;
//import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.ObjectMapper;
//import com.fasterxml.jackson.core.JsonGenerationException;
//import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.ObjectMapper;

//
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.net.ssl.HttpsURLConnection;

// JAX RS
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;

// i3
import com.i3solutions.form.beans.FormRenderConfiguration;	// holds configuration settings for rendering assessment and/or survey data

// WCM 
import com.ibm.workplace.wcm.api.Content;
import com.ibm.workplace.wcm.api.LibraryHTMLComponent;
//import com.ibm.workplace.wcm.api.LinkComponent;
import com.ibm.workplace.wcm.api.OptionSelectionComponent;
import com.ibm.workplace.wcm.api.TextComponent;
import com.ibm.workplace.wcm.api.WcmContentService;
import com.ibm.workplace.wcm.api.Workspace;
import com.ibm.workplace.wcm.api.query.Query;
import com.ibm.workplace.wcm.api.query.QueryService;
import com.ibm.workplace.wcm.api.query.ResultIterator;
import com.ibm.workplace.wcm.api.query.Selectors;







// Freemarker
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;


@Path("/formRender")
//@Stateless
public class FormRenderService {
	
	public static final Logger logger = Logger.getLogger(FormRenderService.class.getName());
	public static final String AssetURL = "/forms/secure/org/run/service/ContentStorageService/";
	
	@Context
	UriInfo uriInfo;
	Request request;
	Response response;
	
	@GET
	@Path("{assessmentTypeName}/{appId}/{formId}/{recordId}/{templateName}") 
	@Produces(MediaType.TEXT_HTML)
	public Response getFormRender(	
			@PathParam("assessmentTypeName") String assessmentTypeName, 
			@PathParam("appId") String assessmentAppId, 
			@PathParam("formId") String assessmentFormId, 
			@PathParam("recordId") String assessmentRecordId, 
			@PathParam("templateName") String templateName,
			@Context SecurityContext security,
			@CookieParam("LtpaToken2") Cookie LTPACookie) {
		
		String testStr = "getFormRender called with:" 
				+ "\n assessmentTypeName = " + assessmentTypeName
				+ "\n appId = " + assessmentAppId
				+ "\n formId = " + assessmentFormId
				+ "\n recordId = " + assessmentRecordId
				+ "\n templateName = " + templateName;
		logger.info(testStr);
		
        StringWriter templateOutput = new StringWriter();
		try {
			// Construct and initialize Context
		    InitialContext ctx = new InitialContext();
		    WcmContentService webContentService;
		    
		    // Retrieve WcmContentService using JNDI name
		    webContentService = (WcmContentService) ctx.lookup("portal:service/wcm/WcmContentService");

			// --------------------------------------------------
			// TODO: Next line of code may not work for service and might need to be rewritten
			// --------------------------------------------------
		    Workspace workspace = webContentService.getRepository().getWorkspace( security.getUserPrincipal() );
//		    Workspace workspace = webContentService.getRepository().getWorkspace( (Principal) request.getUserPrincipal());
		    logger.info("Workspace: " + workspace);
		    QueryService queryService = workspace.getQueryService();

			FormRenderConfiguration formRenderConfig = getFormRenderConfiguration(
					queryService, 
					assessmentTypeName, assessmentAppId, assessmentFormId, assessmentRecordId, 
					null, null, null, 
					templateName);
				
		    if(formRenderConfig.isConfigSet()) {
		    	Map<String, Object> fieldMap = getAssessmentData(request, formRenderConfig, LTPACookie);
	            
	            //Process the template from datasource
		    	// old code
//		        Configuration cfg = new Configuration(Configuration.VERSION_2_3_25);
//	            cfg.setLogTemplateExceptions(false);
		    	Configuration cfg = new Configuration();
		        cfg.setDefaultEncoding("UTF-8");
		        cfg.setLocale(Locale.US);		    	
	            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

	            Template template = new Template(
	            			assessmentTypeName + "-" + templateName, 
	            			new StringReader(formRenderConfig.getDisplayTemplate()), 
	            			cfg);
	            template.process(fieldMap, templateOutput);
	            
	            
			    return Response.ok(templateOutput.toString()).build();
	        } else {
	        	return Response.status(Response.Status.SEE_OTHER).entity("Error processing request: Form Render Configuration Failed.").build();
	        }
		} catch (NamingException ne) {
			ne.printStackTrace();
			return Response.status(Response.Status.SEE_OTHER).entity("Error processing request.").build();
		} catch(Exception ex) {
			ex.printStackTrace();
			return Response.status(Response.Status.SEE_OTHER).entity("Error processing request.").build();
		}

	} // end Response getFormRender


	//--------------------------------------------------------------------------------
	/* 
	 * getFormRenderConfiguration
	 * 		Get the config settings for rendering FEB Form data for CIMDPS Assess,emts and Surveys 
	 * 		QueryService queryService:	
	 * 		String assessmentTypeName:			The kind of form data to render, uses the WCM Form Name
	 *		String assessmentAppId:			The FEB application Id for the assessment data being rendered
	 *		String assessmentFormId:		The FEB form Id for the assessment data being rendered
	 *		String assessmentRecordId:		The FEB record Id for the assessment data being rendered
	 *		String assessmentAppId:			The FEB application Id for the survey data being rendered
	 *		String assessmentFormId:		The FEB form Id for the survey data being rendered
	 *		String assessmentRecordId:		The FEB record Id for the survey data being rendered
	 *		String displayTemplateName:	
	 * 
	 * returns FormRenderConfiguration 
	 */
	//--------------------------------------------------------------------------------
	private FormRenderConfiguration getFormRenderConfiguration(
				QueryService queryService, 
				String assessmentTypeName, 
				String assessmentAppId, 
				String assessmentFormId, 
				String assessmentRecordId, 
				String surveyAppId, 
				String surveyFormId, 
				String surveyRecordId, 
				String displayTemplateName) {

		logger.info("getFormRenderConfiguration called with:"
				+ "\n\t queryService = " + queryService 
				+ "\n\t assessmentTypeName = " + assessmentTypeName 
				+ "\n\t assessmentAppId = " + assessmentAppId
				+ "\n\t assessmentFormId = " + assessmentFormId
				+ "\n\t assessmentRecordId = " + assessmentRecordId 
				+ "\n\t surveyAppId = " + surveyAppId
				+ "\n\t surveyFormId = " + surveyFormId
				+ "\n\t surveyRecordId = " + surveyRecordId
				+ "\n\t displayTemplateName = " + displayTemplateName);
		
		FormRenderConfiguration formRenderConfig = new FormRenderConfiguration();
		
		try {
			formRenderConfig.setFormType(assessmentTypeName);
			formRenderConfig.setAssessmentRecordId(assessmentRecordId);
	    	String formDataURL = ""; 
        	TextComponent fieldMappings;
        	OptionSelectionComponent authType;
			
			Query query = queryService.createQuery(Content.class);
            // Lookup Configuration for the assessment and/or survey being rendered 
            query.addSelector(Selectors.nameEquals(assessmentTypeName));
            @SuppressWarnings("rawtypes")
			ResultIterator resultIterator = queryService.execute(query);
            if (resultIterator.hasNext()) {
            	
            	// The WCM content item that defines the render settings
            	Content config = (Content) resultIterator.next();	
//            	LinkComponent dataEndPointURL;
            	
            	OptionSelectionComponent includeAssessmentData = (OptionSelectionComponent)config.getComponentByReference("Details");
			    formRenderConfig.setIncludeAssessmentData(includeAssessmentData.getSelections()[0].equalsIgnoreCase("Yes"));
			    logger.info("Include Assessment Data in Render? " + formRenderConfig.includeAssessmentData());
			    
			    // ----------------------------------------
			    // Get Assessment Rendering Settings
			    // ----------------------------------------
			    if(formRenderConfig.includeAssessmentData()) {
			    	// TODO: The endpoint URL should have three arguments provided: appId, formId, and recordId
			    	// TODO: Code should be refactored to use all three instead of hard coding appId and formId
			        
			    	formDataURL = "/forms-basic/secure/org/data/" 
			        		+ assessmentAppId 
			        		+ "/" + assessmentFormId + "/" 
			        		+ assessmentRecordId 
			        		+ "?itemOnly=true&format=application%2fjson";
			    	formRenderConfig.setAssessmentDataEndPointURL(formDataURL);
				    logger.info("Assessment Data Endpoint URL: " + formRenderConfig.getAssessmentDataEndPointURL());

				    // Original code commented out till it's refactored
				    //dataEndPointURL = (LinkComponent)config.getComponentByReference("Endpoint");
			        //formRenderConfig.setEndPoint(dataEndPointURL.getURL().replaceFirst("\\{recordId}", recordId));
				    //endPoint = endPoint.replaceFirst("\\{recordId}", recordId);
				    //logger.info("Assessment Data Endpoint URL: " + formRenderConfig.getAssessmentDataEndPointURL());
				    
				    fieldMappings = (TextComponent)config.getComponentByReference("Field Mapping");
				    formRenderConfig.setAssessmentFieldMappings(fieldMappings.getText());
				    logger.info("Assessment Field Mappings: " + formRenderConfig.getAssessmentFieldMappings());
				    
				    authType = (OptionSelectionComponent)config.getComponentByReference("Endpoint Authentication");
				    formRenderConfig.setAssessmentAuthType(authType.getSelections()[0]);
				    logger.info("Assessment Auth Type: " + formRenderConfig.getAssessmentAuthType());
			    } // end if(formRenderConfig.includeAssessmentData())
			    
			    OptionSelectionComponent includeSurveyData = (OptionSelectionComponent)config.getComponentByReference("Header");
			    formRenderConfig.setIncludeSurveyData(includeSurveyData.getSelections()[0].equalsIgnoreCase("Yes"));
			    logger.info("Include Survey Data in Render? " + formRenderConfig.includeSurveyData());
			    
			    // ----------------------------------------
			    // Get Survey Rendering Settings
			    // ----------------------------------------
			    if(formRenderConfig.includeSurveyData()) {
			    	formDataURL = "/forms-basic/secure/org/data/" 
			        		+ surveyAppId 
			        		+ "/" + surveyFormId + "/" 
			        		+ surveyRecordId 
			        		+ "?itemOnly=true&format=application%2fjson";
			    	formRenderConfig.setSurveyDataEndPointURL(formDataURL);
				    logger.info("Survey Data Endpoint URL Endpoint: " + formRenderConfig.getSurveyDataEndPointURL());

//			    	dataEndPointURL = (LinkComponent)config.getComponentByReference("Header Endpoint");
//			    	formRenderConfig.setSurveyDataEndPointURL(dataEndPointURL.getURL());
			    	//headerEndPoint = headerEndPoint.replaceFirst("\\{recordId}", surveyId);
//				    logger.info("Header Endpoint: " + formRenderConfig.getSurveyDataEndPointURL());
				    
				    fieldMappings = (TextComponent)config.getComponentByReference("Header Field Mapping");
				    formRenderConfig.setSurveyFieldMappings(fieldMappings.getText());
				    logger.info("Survey Field Mappings: " + formRenderConfig.getSurveyFieldMappings());
				    
				    authType = (OptionSelectionComponent)config.getComponentByReference("Header Authentication");
				    formRenderConfig.setSurveyAuthType(authType.getSelections()[0]);
				    logger.info("Survey Auth Type: " + formRenderConfig.getSurveyAuthType());
			    } // end if(formRenderConfig.includeSurveyData())
			    
			    // Get the Render Template from WCM
			    Query displayTemplateNameQuery = queryService.createQuery(LibraryHTMLComponent.class);
	            displayTemplateNameQuery.addSelector(Selectors.nameEquals(assessmentTypeName + "-" + displayTemplateName));
	            resultIterator = queryService.execute(displayTemplateNameQuery);
	            if (resultIterator.hasNext()) {
	            	LibraryHTMLComponent template = (LibraryHTMLComponent) resultIterator.next();
	            	formRenderConfig.setDisplayTemplate(template.getHTML());
				    logger.info("Display Template HTML: " + formRenderConfig.getDisplayTemplate());
				 
				    formRenderConfig.setConfigSet(true);
	            } // end if (resultIterator.hasNext())
            } // end if (resultIterator.hasNext())
		} catch(Exception ex) {
			 ex.printStackTrace();
		}
		
		return formRenderConfig;
		
	} // end FormRenderConfiguration getFormRenderConfiguration(QueryService queryService, String assessmentTypeName, String recordId, String displayTemplateName)
	
	//--------------------------------------------------------------------------------
	/* 
	 * getAssessmentData
	 * 		Fetch Assessment and/or Survey Form Data from FEB
	 * 
	 * 		Request request
	 * 		FormRenderConfiguration formRenderConfig
	 * 		Cookie LTPACookie
	 * 
	 * returns Map<String, Object> 
	 */
	//--------------------------------------------------------------------------------
	private Map<String, Object> getAssessmentData(Request request, FormRenderConfiguration formRenderConfig, Cookie LTPACookie) {
		Map<String, Object> fieldMap = new HashMap<String, Object>();
		
		try {
            ObjectMapper objectMapper = new ObjectMapper();

		    // ----------------------------------------
		    // Get Assessment Data
		    // ----------------------------------------
			if(formRenderConfig.includeAssessmentData()) {
				URL url = new URL(formRenderConfig.getAssessmentDataEndPointURL());
				HttpsURLConnection urlConnection = (HttpsURLConnection)url.openConnection();
				urlConnection.setRequestMethod("GET");
	            
				if("Use LTPAToken".equalsIgnoreCase(formRenderConfig.getAssessmentAuthType())) {
					String ltpaCookieKey = "LtpaToken=" + getLtpaCookieKey(LTPACookie);
					urlConnection.setRequestProperty("Cookie", ltpaCookieKey);
				}
	            
				urlConnection.connect();
	            int code = urlConnection.getResponseCode();
	            logger.info("Response Code: " + code);
				InputStream is = urlConnection.getInputStream();
	            
	            JsonNode endPointNode = objectMapper.readValue(is, JsonNode.class);
	            logger.info("App ID: " + endPointNode.get("application_uid").getTextValue());
	            JsonNode itemsNode = endPointNode.get("items").get(0);
	            logger.info("Items Node: " + itemsNode);
	            
	            JsonNode fieldMappingsNode = objectMapper.readValue(formRenderConfig.getAssessmentFieldMappings(), JsonNode.class);
	            logger.info("Assessment Field Mappings: " + fieldMappingsNode);
	            
	            Iterator<String> assessmentFieldIterator = fieldMappingsNode.getFieldNames();
	            while(assessmentFieldIterator.hasNext()) {
	            	String recordField = assessmentFieldIterator.next();
	            	
	            	if(recordField.equalsIgnoreCase("pictureURL")) {
	            		JsonNode pictureNode = itemsNode.get(fieldMappingsNode.get(recordField).getTextValue());
	            		String pictureId = pictureNode.get("id").getTextValue();
	            		String pictureUrl = "";
	            		if(!("".equalsIgnoreCase(pictureId))) {
	            			pictureUrl = AssetURL + pictureId;
	            		}
	            		fieldMap.put(recordField, pictureUrl);
	            	} else {
		            	logger.info("Source field: " + fieldMappingsNode.get(recordField).getTextValue());
		            	if(itemsNode.get(fieldMappingsNode.get(recordField).getTextValue()) != null) {
			            	String fieldValue = itemsNode.get(fieldMappingsNode.get(recordField).getTextValue()).getTextValue();
			            	fieldMap.put(recordField, fieldValue.trim());
		            	}
	            	} // end else
	            } // end while
            } // end if(formRenderConfig.includeAssessmentData())
            
		    // ----------------------------------------
		    // Get Survey Data
		    // ----------------------------------------
            if(formRenderConfig.includeSurveyData()) {
            	String surveyId = "";
            	if(formRenderConfig.includeAssessmentData()) {
            		surveyId = (String)fieldMap.get("headerId");
            	} else {
            		surveyId = formRenderConfig.getSurveyRecordId();
            	}
            	
            	logger.info("Survey Id: " + surveyId);
            	logger.info("Survey Endpoint URL: " + formRenderConfig.getSurveyDataEndPointURL());
//            	logger.info("Survey Endpoint URL: " + formRenderConfig.getHeaderEndPoint().replaceFirst("\\{recordId}", surveyId));
//            	URL surveyDataEndpointURL = new URL(formRenderConfig.getHeaderEndPoint().replaceFirst("\\{recordId}", surveyId));
            	URL surveyDataEndpointURL = new URL(formRenderConfig.getSurveyDataEndPointURL());
                HttpsURLConnection surveyUrlConnection = (HttpsURLConnection)surveyDataEndpointURL.openConnection();
                surveyUrlConnection.setRequestMethod("GET");
                
                if("Use LTPAToken".equalsIgnoreCase(formRenderConfig.getSurveyAuthType())) {
    	            String ltpaCookieKey = "LtpaToken=" + getLtpaCookieKey(LTPACookie);
    	            surveyUrlConnection.setRequestProperty("Cookie", ltpaCookieKey);
                }
                
                surveyUrlConnection.connect();
                int surveyDataFetchResponseCode = surveyUrlConnection.getResponseCode();
                logger.info("Survey Response Code: " + surveyDataFetchResponseCode);
    			InputStream surveyInputStream = surveyUrlConnection.getInputStream();
    			
    			JsonNode surveyEndPointNode = objectMapper.readValue(surveyInputStream, JsonNode.class);
                logger.info("Survey App ID: " + surveyEndPointNode.get("application_uid").getTextValue());
                JsonNode surveyItemsNode = surveyEndPointNode.get("items").get(0);
                logger.info("Survey Items Node: " + surveyItemsNode);
                
                
                
                JsonNode surveyFieldMappingsNode = objectMapper.readValue(formRenderConfig.getSurveyFieldMappings(), JsonNode.class);
                logger.info("Header Field Mappings Node: " + surveyFieldMappingsNode);
                
                Iterator<String> surveyFieldIterator = surveyFieldMappingsNode.getFieldNames();
                while(surveyFieldIterator.hasNext()) {
                	String recordField = surveyFieldIterator.next();
                	if(recordField.equalsIgnoreCase("attachments")) {
                		JsonNode tableNode = surveyItemsNode.get(surveyFieldMappingsNode.get(recordField).getTextValue());
                		logger.info("F_Table items: " + tableNode.get("items").toString());
                		@SuppressWarnings("unchecked")
						ArrayList<Object> attachmentList = objectMapper.readValue(tableNode.get("items").toString(), ArrayList.class);
                		ArrayList<Object> newAttachmentList = new ArrayList<Object>();
                		for(Object attachment : attachmentList) {
                			@SuppressWarnings("unchecked")
							Map<String, Object> attachmentMap = (Map<String, Object>)attachment;
                			@SuppressWarnings("unchecked")
							Map<String, Object> fileMap = (Map<String, Object>)attachmentMap.get("F_FileAttachment");
                			String attachmentUrl = AssetURL + fileMap.get("id");
                			String fileName = (String)fileMap.get("fileName");
                			logger.info("Header attachment url: " + attachmentUrl);
                			Map<String, String> newAttachmentMap = new HashMap<String, String>();
                			newAttachmentMap.put("attachmentURL", attachmentUrl);
                			newAttachmentMap.put("attachmentFileName", fileName);
                			if(isImage(fileName)) {
                				newAttachmentMap.put("attachmentIsImage", "true");
                			} else {
                				newAttachmentMap.put("attachmentIsImage", "false");
                			}
                			newAttachmentMap.put("attachmentNotes", (String)attachmentMap.get("F_FileNotes"));
                			newAttachmentList.add(newAttachmentMap);
                		} // end for(Object attachment : attachmentList)
                		fieldMap.put(recordField, newAttachmentList);
                	} else {
    	            	logger.info("Survey Source field: " + surveyFieldMappingsNode.get(recordField).getTextValue());
    	            	String fieldValue = surveyItemsNode.get(surveyFieldMappingsNode.get(recordField).getTextValue()).getTextValue();
    	            	fieldMap.put(recordField, fieldValue.trim());
                	} // end else
                } // end while(surveyFieldIterator.hasNext())
            	
            }
            
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		return fieldMap;
	} // end getBBCardData(PortletRequest request, BBCardConfiguration formRenderConfig)
	
	private String getLtpaCookieKey(Cookie ltpaCookie) {
		String ltpaTokenKey = "";
		if (ltpaCookie != null) {
			ltpaTokenKey = ltpaCookie.getValue();
		}
		
		return ltpaTokenKey;
	} // end getLtpaCookieKey(Cookie ltpaCookie)

	private boolean isImage(String fileName) {
		String[] supportedImageTypes = new String[] {"jpg", "jpeg", "png", "gif", "svg", "bmp", "ico"};
		
		String extString = "";
		int i = fileName.lastIndexOf('.');
		if (i > 0) {
			extString = fileName.substring(i+1);
		}
		
		return Arrays.asList(supportedImageTypes).contains(extString.toLowerCase());
		
	} // end isImage(String fileName)

} // end class FormRenderService