package com.i3solutions.form.entity;

import java.util.List;
import com.i3solutions.form.entity.TableItem;;

public class Table {
	private String innerFormId = "";
	private List<TableItem> items;

	// getters
	public String getInnerFormId() {
		return innerFormId;
	}
	public List<TableItem> getItems() {
		return items;
	}
	
	// Setters
	public void setInnerFormId(String innerFormIdVal) {
		innerFormId = innerFormIdVal;
	}
	
	public void getItems(List<TableItem> itemsVal) {
		items = itemsVal;
	}
	
} // end class Table
