package com.i3solutions.form.entity;

import com.i3solutions.form.entity.Person;

import java.util.List;

public class CivilianEngagement {
	private String application_uid = "";
	private String formId = "";
	private String lastModified = "";
	private List<Person> lastModifiedBy;
	private String created = "";
	private List<Person> createdBy;
	private String draft_ownerid = "";
	private String flowState = "";
	private int id = 0;
	private String uid = "";
	private boolean F_UsePrevAssessment = false;
	private int F_NumericSlider1 = 0;
	private String F_IntroductionName = "";
	private String F_SingleLine1 = "";
	private String F_Age = "";
	private String F_Sex = "";
	private List<Picture> F_Picture;
	private String F_Height = "";
	private String F_Weight = "";
	private String F_PlaceAssessed = "";
	private String F_Latitude = "";
	private String F_UID = "";		// name of person who did the assessment
	private String F_Longitude = "";
	private String F_AssessmentDate = "";
	private String F_LocDesc = "";
	private String F_Region = "";
	private String F_Dept = "";
	private String F_Admin4 = "";
	private String F_MGRS = "";
	private String F_RecID = "";		// record id
	private String F_AppID = "";		// app id
	private String F_AppName = "";	// app name
	private String F_FormID = "";	// form id
	private String F_FormURL = "";
	private String F_HeaderID = "";	// survey id
	private String F_PreviousID = "";
	private String F_NumericSlider2 = "";
	private String F_Education = "";
	private String F_ReceiveEducation = "";	// specific, unknown or not applicable
	private String F_LanguageSpeak = "";		// specific, unknown or not applicable
	private String F_UnderstandLanguage = "";// specific, unknown or not applicable
	private String F_Handicaps = "";			// specific, unknown or not applicable
	private String F_Race = "";
	private String F_SelectOne10 = "";
	private String F_SocialClass = "";
	private int F_NumericSlider3 = 0;
	private String F_IsEmployed = "";
	private String F_Employer = "";
	private String F_Occupation = "";
	private String F_Transportation = "";
	private String F_IsGovtEmployee = "";
	private String F_WhereGovtEmployee = "";
	private String F_IsSoldier = "";
	private String F_Rank = "";
	private String F_SelectOne11 = "";
	private String F_BirthPlace = "";
	private String F_SelectOne12 = "";
	private int F_NumericSlider4 = 0;
	private String F_HonestyIntegrity = "";
	private String F_ExtrovertIntrovert = "";
	private String F_QuietTalkitive = "";
	private String F_DiscreetIndiscreet = "";
	private String F_CourFearCautious = "";
	private String F_LazyAmbitious = "";
	private String F_PersonalityComplex = "";
	private String F_ReligiousBeliefs = "";
	private String F_Drinking = "";
	private String F_Comments = "";
	private boolean F_ReadySubmit = false;
	private String F_MetaTags = "";
	private String F_FinalSummary = "";
	private Table F_Table1;
	private Table F_SpeakLanguage;
	private Table F_TbaleUnderstandLang;
	private Table F_HandicapsTable;
	private Table F_Employment;

	// Getters
	public String getApplication_uid() {
		return application_uid;
	}
	
	public String getFormId() {
		return formId;
	}
	
	public String getLastModified() {
		return lastModified;
	}
	
	public List<Person> getLastModifiedBy() {
		return lastModifiedBy;
	}
	
	public String getCreated() {
		return created;
	}
	
	public List<Person> getCreatedBy() {
		return createdBy;
	}
	
	public String getDraft_ownerid() {
		return draft_ownerid;
	}
	
	public String getFlowState() {
		return flowState;
	}
	
	public int getId() {
		return id;
	}
	
	public String getUid() {
		return uid;
	}
	
	public boolean getF_UsePrevAssessment() {
		return F_UsePrevAssessment;
	}
	
	public int getF_NumericSlider1() {
		return F_NumericSlider1;
	}
	
	public String getF_IntroductionName() {
		return F_IntroductionName;
	}
	
	public String getF_SingleLine1() {
		return F_SingleLine1;
	}
	
	public String getF_Age() {
		return F_Age;
	}
	
	public String getF_Sex() {
		return F_Sex;
	}
	
	public List<Picture> getF_Picture() {
		return F_Picture;
	}
	
	public String getF_Height() {
		return F_Height;
	}
	
	public String getF_Weight() {
		return F_Weight;
	}
	
	public String getF_PlaceAssessed() {
		return F_PlaceAssessed;
	}
	
	public String getF_Latitude() {
		return F_Latitude;
	}
	
	public String getF_UID() {
		return F_UID;
	}
	
	public String getF_Longitude() {
		return F_Longitude;
	}
	
	public String getF_AssessmentDate() {
		return F_AssessmentDate;
	}
	
	public String getF_LocDesc() {
		return F_LocDesc;
	}
	
	public String getF_Region() {
		return F_Region;
	}
	
	public String getF_Dept() {
		return F_Dept;
	}
	
	public String getF_Admin4() {
		return F_Admin4;
	}
	
	public String getF_MGRS() {
		return F_MGRS;
	}
	
	public String getF_RecID() {
		return F_RecID;
	}
	
	public String getF_AppID() {
		return F_AppID;
	}
	
	public String getF_AppName() {
		return F_AppName;
	}
	
	public String getF_FormID() {
		return F_FormID;
	}
	
	public String getF_FormURL() {
		return F_FormURL;
	}
	
	public String getF_HeaderID() {
		return F_HeaderID;
	}
	
	public String getF_PreviousID() {
		return F_PreviousID;
	}
	
	public String getF_NumericSlider2() {
		return F_NumericSlider2;
	}
	
	public String getF_Education() {
		return F_Education;
	}
	
	public String getF_ReceiveEducation() {
		return F_ReceiveEducation;
	}
	
	public String getF_LanguageSpeak() {
		return F_LanguageSpeak;
	}
	
	public String getF_UnderstandLanguage() {
		return F_UnderstandLanguage;
	}
	
	public String getF_Handicaps() {
		return F_Handicaps;
	}
	
	public String getF_Race() {
		return F_Race;
	}
	
	public String getF_SelectOne10() {
		return F_SelectOne10;
	}
	
	public String getF_SocialClass() {
		return F_SocialClass;
	}
	
	public int getF_NumericSlider3() {
		return F_NumericSlider3;
	}
	
	public String getF_IsEmployed() {
		return F_IsEmployed;
	}
	
	public String getF_Employer() {
		return F_Employer;
	}
	
	public String getF_Occupation() {
		return F_Occupation;
	}
	
	public String getF_Transportation() {
		return F_Transportation;
	}
	
	public String getF_IsGovtEmployee() {
		return F_IsGovtEmployee;
	}
	
	public String getF_WhereGovtEmployee() {
		return F_WhereGovtEmployee;
	}
	
	public String getF_IsSoldier() {
		return F_IsSoldier;
	}
	
	public String getF_Rank() {
		return F_Rank;
	}
	
	public String getF_SelectOne11() {
		return F_SelectOne11;
	}
	
	public String getF_BirthPlace() {
		return F_BirthPlace;
	}
	
	public String getF_SelectOne12() {
		return F_SelectOne12;
	}
	
	public int getF_NumericSlider4() {
		return F_NumericSlider4;
	}
	
	public String getF_HonestyIntegrity() {
		return F_HonestyIntegrity;
	}
	
	public String getF_ExtrovertIntrovert() {
		return F_ExtrovertIntrovert;
	}
	
	public String getF_QuietTalkitive() {
		return F_QuietTalkitive;
	}
	
	public String getF_DiscreetIndiscreet() {
		return F_DiscreetIndiscreet;
	}
	
	public String getF_CourFearCautious() {
		return F_CourFearCautious;
	}
	
	public String getF_LazyAmbitious() {
		return F_LazyAmbitious;
	}
	
	public String getF_PersonalityComplex() {
		return F_PersonalityComplex;
	}
	
	public String getF_ReligiousBeliefs() {
		return F_ReligiousBeliefs;
	}
	
	public String getF_Drinking() {
		return F_Drinking;
	}
	
	public String getF_Comments() {
		return F_Comments;
	}
	
	public boolean getF_ReadySubmit() {
		return F_ReadySubmit;
	}
	
	public String getF_MetaTags() {
		return F_MetaTags;
	}
	
	public String getF_FinalSummary() {
		return F_FinalSummary;
	}
	
	public Table getF_Table1() {
		return F_Table1;
	}
	
	public Table getF_SpeakLanguage() {
		return F_SpeakLanguage;
	}
	
	public Table getF_TbaleUnderstandLang() {
		return F_TbaleUnderstandLang;
	}
	
	public Table getF_HandicapsTable() {
		return F_HandicapsTable;
	}
	
	public Table getF_Employment() {
		return F_Employment;
	}
	
	
	// Setters
	public void setApplication_uid(String application_uidVal) {
		application_uid = application_uidVal;
	}
	
	public void setFormId(String formIdVal) {
		formId = formIdVal;
	}
	
	public void setLastModified(String lastModifiedVal) {
		lastModified = lastModifiedVal;
	}
	
	public void setLastModifiedBy(List<Person> lastModifiedByVal) {
		lastModifiedBy = lastModifiedByVal;
	}
	
	public void setCreated(String createdVal) {
		created = createdVal;
	}
	
	public void setCreatedBy(List<Person> createdByVal) {
		createdBy = createdByVal;
	}
	
	public void setDraft_ownerid(String draft_owneridVal) {
		draft_ownerid = draft_owneridVal;
	}
	
	public void setFlowState(String flowStateVal) {
		flowState = flowStateVal;
	}
	
	public void setId(int idVal) {
		id = idVal;
	}
	
	public void setUid(String uidVal) {
		uid = uidVal;
	}
	
	public void setF_UsePrevAssessment(boolean F_UsePrevAssessmentVal) {
		F_UsePrevAssessment = F_UsePrevAssessmentVal;
	}
	
	public void setF_NumericSlider1(int F_NumericSlider1Val) {
		F_NumericSlider1 = F_NumericSlider1Val;
	}
	
	public void setF_IntroductionName(String F_IntroductionNameVal) {
		F_IntroductionName = F_IntroductionNameVal;
	}
	
	public void setF_SingleLine1(String F_SingleLine1Val) {
		F_SingleLine1 = F_SingleLine1Val;
	}
	
	public void setF_Age(String F_AgeVal) {
		F_Age = F_AgeVal;
	}
	
	public void setF_Sex(String F_SexVal) {
		F_Sex = F_SexVal;
	}
	
	public void setF_Picture(List<Picture> F_PictureVal) {
		F_Picture = F_PictureVal;
	}
	
	public void setF_Height(String F_HeightVal) {
		F_Height = F_HeightVal;
	}
	
	public void setF_Weight(String F_WeightVal) {
		F_Weight = F_WeightVal;
	}
	
	public void setF_PlaceAssessed(String F_PlaceAssessedVal) {
		F_PlaceAssessed = F_PlaceAssessedVal;
	}
	
	public void setF_Latitude(String F_LatitudeVal) {
		F_Latitude = F_LatitudeVal;
	}
	
	public void setF_UID(String F_UIDVal) {
		F_UID = F_UIDVal;
	}
	
	public void setF_Longitude(String F_LongitudeVal) {
		F_Longitude = F_LongitudeVal;
	}
	
	public void setF_AssessmentDate(String F_AssessmentDateVal) {
		F_AssessmentDate = F_AssessmentDateVal;
	}
	
	public void setF_LocDesc(String F_LocDescVal) {
		F_LocDesc = F_LocDescVal;
	}
	
	public void setF_Region(String F_RegionVal) {
		F_Region = F_RegionVal;
	}
	
	public void setF_Dept(String F_DeptVal) {
		F_Dept = F_DeptVal;
	}
	
	public void setF_Admin4(String F_Admin4Val) {
		F_Admin4 = F_Admin4Val;
	}
	
	public void setF_MGRS(String F_MGRSVal) {
		F_MGRS = F_MGRSVal;
	}
	
	public void setF_RecID(String F_RecIDVal) {
		F_RecID = F_RecIDVal;
	}
	
	public void setF_AppID(String F_AppIDVal) {
		F_AppID = F_AppIDVal;
	}
	
	public void setF_AppName(String F_AppNameVal) {
		F_AppName = F_AppNameVal;
	}
	
	public void setF_FormID(String F_FormIDVal) {
		F_FormID = F_FormIDVal;
	}
	
	public void setF_FormURL(String F_FormURLVal) {
		F_FormURL = F_FormURLVal;
	}
	
	public void setF_HeaderID(String F_HeaderIDVal) {
		F_HeaderID = F_HeaderIDVal;
	}
	
	public void setF_PreviousID(String F_PreviousIDVal) {
		F_PreviousID = F_PreviousIDVal;
	}
	
	public void setF_NumericSlider2(String F_NumericSlider2Val) {
		F_NumericSlider2 = F_NumericSlider2Val;
	}
	
	public void setF_Education(String F_EducationVal) {
		F_Education = F_EducationVal;
	}
	
	public void setF_ReceiveEducation(String F_ReceiveEducationVal) {
		F_ReceiveEducation = F_ReceiveEducationVal;
	}
	
	public void setF_LanguageSpeak(String F_LanguageSpeakVal) {
		F_LanguageSpeak = F_LanguageSpeakVal;
	}
	
	public void setF_UnderstandLanguage(String F_UnderstandLanguageVal) {
		F_UnderstandLanguage = F_UnderstandLanguageVal;
	}
	
	public void setF_Handicaps(String F_HandicapsVal) {
		F_Handicaps = F_HandicapsVal;
	}
	
	public void setF_Race(String F_RaceVal) {
		F_Race = F_RaceVal;
	}
	
	public void setF_SelectOne10(String F_SelectOne10Val) {
		F_SelectOne10 = F_SelectOne10Val;
	}
	
	public void setF_SocialClass(String F_SocialClassVal) {
		F_SocialClass = F_SocialClassVal;
	}
	
	public void setF_NumericSlider3(int F_NumericSlider3Val) {
		F_NumericSlider3 = F_NumericSlider3Val;
	}
	
	public void setF_IsEmployed(String F_IsEmployedVal) {
		F_IsEmployed = F_IsEmployedVal;
	}
	
	public void setF_Employer(String F_EmployerVal) {
		F_Employer = F_EmployerVal;
	}
	
	public void setF_Occupation(String F_OccupationVal) {
		F_Occupation = F_OccupationVal;
	}
	
	public void setF_Transportation(String F_TransportationVal) {
		F_Transportation = F_TransportationVal;
	}
	
	public void setF_IsGovtEmployee(String F_IsGovtEmployeeVal) {
		F_IsGovtEmployee = F_IsGovtEmployeeVal;
	}
	
	public void setF_WhereGovtEmployee(String F_WhereGovtEmployeeVal) {
		F_WhereGovtEmployee = F_WhereGovtEmployeeVal;
	}
	
	public void setF_IsSoldier(String F_IsSoldierVal) {
		F_IsSoldier = F_IsSoldierVal;
	}
	
	public void setF_Rank(String F_RankVal) {
		F_Rank = F_RankVal;
	}
	
	public void setF_SelectOne11(String F_SelectOne11Val) {
		F_SelectOne11 = F_SelectOne11Val;
	}
	
	public void setF_BirthPlace(String F_BirthPlaceVal) {
		F_BirthPlace = F_BirthPlaceVal;
	}
	
	public void setF_SelectOne12(String F_SelectOne12Val) {
		F_SelectOne12 = F_SelectOne12Val;
	}
	
	public void setF_NumericSlider4(int F_NumericSlider4Val) {
		F_NumericSlider4 = F_NumericSlider4Val;
	}
	
	public void setF_HonestyIntegrity(String F_HonestyIntegrityVal) {
		F_HonestyIntegrity = F_HonestyIntegrityVal;
	}
	
	public void setF_ExtrovertIntrovert(String F_ExtrovertIntrovertVal) {
		F_ExtrovertIntrovert = F_ExtrovertIntrovertVal;
	}
	
	public void setF_QuietTalkitive(String F_QuietTalkitiveVal) {
		F_QuietTalkitive = F_QuietTalkitiveVal;
	}
	
	public void setF_DiscreetIndiscreet(String F_DiscreetIndiscreetVal) {
		F_DiscreetIndiscreet = F_DiscreetIndiscreetVal;
	}
	
	public void setF_CourFearCautious(String F_CourFearCautiousVal) {
		F_CourFearCautious = F_CourFearCautiousVal;
	}
	
	public void setF_LazyAmbitious(String F_LazyAmbitiousVal) {
		F_LazyAmbitious = F_LazyAmbitiousVal;
	}
	
	public void setF_PersonalityComplex(String F_PersonalityComplexVal) {
		F_PersonalityComplex = F_PersonalityComplexVal;
	}
	
	public void setF_ReligiousBeliefs(String F_ReligiousBeliefsVal) {
		F_ReligiousBeliefs = F_ReligiousBeliefsVal;
	}
	
	public void setF_Drinking(String F_DrinkingVal) {
		F_Drinking = F_DrinkingVal;
	}
	
	public void setF_Comments(String F_CommentsVal) {
		F_Comments = F_CommentsVal;
	}
	
	public void setF_ReadySubmit(boolean F_ReadySubmitVal) {
		F_ReadySubmit = F_ReadySubmitVal;
	}
	
	public void setF_MetaTags(String F_MetaTagsVal) {
		F_MetaTags = F_MetaTagsVal;
	}
	
	public void setF_FinalSummary(String F_FinalSummaryVal) {
		F_FinalSummary = F_FinalSummaryVal;
	}
	
	public void setF_Table1(Table F_Table1Val) {
		F_Table1 = F_Table1Val;
	}
	
	public void setF_SpeakLanguage(Table F_SpeakLanguageVal) {
		F_SpeakLanguage = F_SpeakLanguageVal;
	}
	
	public void setF_TbaleUnderstandLang(Table F_TbaleUnderstandLangVal) {
		F_TbaleUnderstandLang = F_TbaleUnderstandLangVal;
	}
	
	public void setF_HandicapsTable(Table F_HandicapsTableVal) {
		F_HandicapsTable = F_HandicapsTableVal;
	}
	
	public void setF_Employment(Table F_EmploymentVal) {
		F_Employment = F_EmploymentVal;
	}
	
} // end class CivilianEngagement
