package com.i3solutions.form.entity;

public class Picture {
	private int id = 0;
	private String fileName = "";
	
	// Getters
	public int getId() {
		return id;
	}

	public String getFileName() {
		return fileName;
	}

	// Setters
	public void setId(int idVal) {
		id = idVal;
	}

	public void setFileName(String fileNameVal) {
		fileName = fileNameVal;
	}

} // end class Picture
