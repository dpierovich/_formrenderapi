package com.i3solutions.form.entity;

/*
 * Class TableItem
 * 		Desc: Defines a FEB table element as returned ib restful request
 * 		Author: D. Pierovich
 * 		Date: 07/22/2017
 */
public class TableItem {
	private String itemName = "";
	private String itemValue = "";
	
	// Getters
	public String getItemName() {
		return itemName;
	}

	public String getItemValue() {
		return itemValue;
	}

	// Setters
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public void setItemValue(String itemValue) {
		this.itemValue = itemValue;
	}
} // end class TableItem
