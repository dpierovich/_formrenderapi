package com.i3solutions.form.entity;

public class Person {
	private String displayName = "";
	private String email = "";
	private String login = "";
	
	public String getDisplayName() {
		return displayName;
	}

	public String getEmail() {
		return email;
	}

	public String getLogin() {
		return login;
	}

	public void setDisplayName(String nameVal) {
		displayName = nameVal;
	}

	public void setEmail(String emailVal) {
		email = emailVal;
	}

	public void setLogin(String loginVal) {
		login = loginVal;
	}

}
